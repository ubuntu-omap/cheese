From 906f6b8449aeb980bcbdc935a5fc8db5a246b99c Mon Sep 17 00:00:00 2001
From: Luciana Fujii Pontello <luciana.fujii@collabora.co.uk>
Date: Mon, 6 Jun 2011 23:16:17 -0300
Subject: [PATCH 49/96] Add image and video stabilization setting

Added cheese-prefs-source-checkbox to control checkboxes that are set as
boolean properties in the camera.

Added settings for image stabilization (mtis - motion triggered image
stabilization) and video stabilization (vstab).
---
 libcheese/cheese-widget.c          |   10 ++
 src/Makefile.am                    |    2 +
 src/cheese-prefs-dialog.c          |   23 ++++
 src/cheese-prefs-source-checkbox.c |  229 ++++++++++++++++++++++++++++++++++++
 src/cheese-prefs-source-checkbox.h |   67 +++++++++++
 5 files changed, 331 insertions(+), 0 deletions(-)
 create mode 100644 src/cheese-prefs-source-checkbox.c
 create mode 100644 src/cheese-prefs-source-checkbox.h

diff --git a/libcheese/cheese-widget.c b/libcheese/cheese-widget.c
index 11b8055..fded21d 100644
--- a/libcheese/cheese-widget.c
+++ b/libcheese/cheese-widget.c
@@ -1,5 +1,7 @@
 /*
  * Copyright © 2009 Bastien Nocera <hadess@hadess.net>
+ * Copyright © 2011 Collabora Ltda
+ *  @author: Luciana Fujii Pontello <luciana.fujii@collabora.co.uk>
  *
  * Licensed under the GNU General Public License Version 2
  *
@@ -309,6 +311,8 @@ setup_camera (CheeseWidget *widget)
   guint                iso_speed;
   guint                focus_weight;
   gboolean             autofocus;
+  gboolean             video_stabilization;
+  gboolean             image_stabilization;
 
   g_object_get (priv->gconf,
                 "gconf_prop_x_resolution", &x_resolution,
@@ -326,6 +330,8 @@ setup_camera (CheeseWidget *widget)
                 "gconf_prop_scene-mode", &scene_mode,
                 "gconf_prop_iso-speed", &iso_speed,
                 "gconf_prop_focusweight", &focus_weight,
+                "gconf_prop_vstab", &video_stabilization,
+                "gconf_prop_mtis", &image_stabilization,
                 NULL);
 
   gdk_threads_enter ();
@@ -359,6 +365,10 @@ setup_camera (CheeseWidget *widget)
     cheese_camera_set_float_property (priv->webcam,
         "ev-compensation", ev_compensation);
     cheese_camera_set_autofocus (priv->webcam, autofocus);
+    cheese_camera_set_boolean_property (priv->webcam,
+                                        "vstab", video_stabilization);
+    cheese_camera_set_boolean_property (priv->webcam,
+                                        "mtis", image_stabilization);
     cheese_camera_set_photography_property (priv->webcam,
         "white-balance-mode", white_balance_mode);
     cheese_camera_set_photography_property (priv->webcam,
diff --git a/src/Makefile.am b/src/Makefile.am
index f309acc..39a1d0d 100644
--- a/src/Makefile.am
+++ b/src/Makefile.am
@@ -47,6 +47,8 @@ cheese_SOURCES = \
 	cheese-prefs-float-scale.h \
 	cheese-prefs-autofocus-checkbox.c \
 	cheese-prefs-autofocus-checkbox.h \
+	cheese-prefs-source-checkbox.c \
+	cheese-prefs-source-checkbox.h \
 	cheese-prefs-dialog.c \
 	cheese-prefs-dialog.h \
 	cheese-prefs-photography-combo.c \
diff --git a/src/cheese-prefs-dialog.c b/src/cheese-prefs-dialog.c
index dc37b0c..7d76967 100644
--- a/src/cheese-prefs-dialog.c
+++ b/src/cheese-prefs-dialog.c
@@ -21,6 +21,7 @@
 #include "cheese-prefs-dialog.h"
 #include "cheese-prefs-photography-combo.h"
 #include "cheese-prefs-autofocus-checkbox.h"
+#include "cheese-prefs-source-checkbox.h"
 
 typedef struct
 {
@@ -39,6 +40,8 @@ typedef struct
   GtkWidget *zoom_scale;
   GtkWidget *ev_compensation_scale;
   GtkWidget *autofocus_checkbutton;
+  GtkWidget *video_stab_checkbutton;
+  GtkWidget *image_stab_checkbutton;
   GtkWidget *burst_repeat;
   GtkWidget *burst_delay;
 
@@ -141,6 +144,10 @@ cheese_prefs_dialog_create_dialog (CheesePrefsDialog *prefs_dialog)
     GTK_WIDGET (gtk_builder_get_object (builder, "ev_scale"));
   prefs_dialog->autofocus_checkbutton =
     GTK_WIDGET (gtk_builder_get_object (builder, "auto_focus_button"));
+  prefs_dialog->video_stab_checkbutton =
+    GTK_WIDGET (gtk_builder_get_object (builder, "video_stab_checkbutton"));
+  prefs_dialog->image_stab_checkbutton =
+    GTK_WIDGET (gtk_builder_get_object (builder, "image_stab_checkbutton"));
   prefs_dialog->burst_repeat = GTK_WIDGET (gtk_builder_get_object (builder, "burst_repeat"));
 
   prefs_dialog->burst_delay = GTK_WIDGET (gtk_builder_get_object (builder, "burst_delay"));
@@ -202,6 +209,8 @@ cheese_prefs_dialog_setup_widgets (CheesePrefsDialog *prefs_dialog)
   CheesePrefsWidget *zoom_widget;
   CheesePrefsWidget *ev_widget;
   CheesePrefsWidget *autofocus_widget;
+  CheesePrefsWidget *video_stab_widget;
+  CheesePrefsWidget *image_stab_widget;
   CheesePrefsWidget *burst_delay_widget;
   CheesePrefsWidget *burst_repeat_widget;
 
@@ -301,6 +310,20 @@ cheese_prefs_dialog_setup_widgets (CheesePrefsDialog *prefs_dialog)
                                            "gconf_prop_autofocus"));
   cheese_prefs_dialog_widgets_add (prefs_dialog->widgets, autofocus_widget);
 
+  video_stab_widget = CHEESE_PREFS_WIDGET (
+      cheese_prefs_source_checkbox_new (prefs_dialog->video_stab_checkbutton,
+                                        prefs_dialog->camera,
+                                        "vstab",
+                                        "gconf_prop_vstab"));
+  cheese_prefs_dialog_widgets_add (prefs_dialog->widgets, video_stab_widget);
+
+  image_stab_widget = CHEESE_PREFS_WIDGET (
+      cheese_prefs_source_checkbox_new (prefs_dialog->image_stab_checkbutton,
+                                        prefs_dialog->camera,
+                                        "mtis",
+                                        "gconf_prop_mtis"));
+  cheese_prefs_dialog_widgets_add (prefs_dialog->widgets, image_stab_widget);
+
   burst_repeat_widget = CHEESE_PREFS_WIDGET (cheese_prefs_burst_spinbox_new (prefs_dialog->burst_repeat,
                                                                              "gconf_prop_burst_repeat"));
 
diff --git a/src/cheese-prefs-source-checkbox.c b/src/cheese-prefs-source-checkbox.c
new file mode 100644
index 0000000..97a18a8
--- /dev/null
+++ b/src/cheese-prefs-source-checkbox.c
@@ -0,0 +1,229 @@
+/*
+ * Copyright © 2011 Collabora Ltda
+ *  @author: Luciana Fujii Pontello <luciana.fujii@collabora.co.uk>
+ *
+ * Licensed under the GNU General Public License Version 2
+ *
+ * This program is free software; you can redistribute it and/or modify
+ * it under the terms of the GNU General Public License as published by
+ * the Free Software Foundation; either version 2 of the License, or
+ * (at your option) any later version.
+ *
+ * This program is distributed in the hope that it will be useful,
+ * but WITHOUT ANY WARRANTY; without even the implied warranty of
+ * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
+ * GNU General Public License for more details.
+ *
+ * You should have received a copy of the GNU General Public License
+ * along with this program.  If not, see <http://www.gnu.org/licenses/>.
+ */
+
+#include <string.h>
+#include <glib.h>
+
+#include <cheese-camera.h>
+#include "cheese-prefs-widget.h"
+#include "cheese-prefs-source-checkbox.h"
+
+enum
+{
+  PROP_0,
+  PROP_PROPERTY,
+  PROP_GCONF_KEY,
+  PROP_CAMERA
+};
+
+typedef struct CheesePrefsSourceCheckboxPrivate
+{
+  CheeseCamera *camera;
+  gchar *gconf_key;
+  gchar *property_name;
+} CheesePrefsSourceCheckboxPrivate;
+
+#define CHEESE_PREFS_SOURCE_CHECKBOX_GET_PRIVATE(o)                     \
+  (G_TYPE_INSTANCE_GET_PRIVATE ((o), CHEESE_TYPE_PREFS_SOURCE_CHECKBOX, \
+                                CheesePrefsSourceCheckboxPrivate))
+
+G_DEFINE_TYPE (CheesePrefsSourceCheckbox, cheese_prefs_source_checkbox,
+               CHEESE_TYPE_PREFS_WIDGET);
+
+static void
+cheese_prefs_source_checkbox_init (CheesePrefsSourceCheckbox *self)
+{
+  CheesePrefsSourceCheckboxPrivate *priv =
+      CHEESE_PREFS_SOURCE_CHECKBOX_GET_PRIVATE (self);
+
+  priv->property_name = NULL;
+  priv->gconf_key = NULL;
+}
+
+static void
+cheese_prefs_source_checkbox_finalize (GObject *object)
+{
+  CheesePrefsSourceCheckbox *self = CHEESE_PREFS_SOURCE_CHECKBOX (object);
+  CheesePrefsSourceCheckboxPrivate *priv =
+      CHEESE_PREFS_SOURCE_CHECKBOX_GET_PRIVATE (self);
+
+  g_free (priv->gconf_key);
+  g_free (priv->property_name);
+
+  G_OBJECT_CLASS (
+      cheese_prefs_source_checkbox_parent_class)->finalize (object);
+}
+
+static void
+cheese_prefs_source_checkbox_toggled (GtkCheckButton *checkbutton,
+    CheesePrefsSourceCheckbox *self)
+{
+  CheesePrefsSourceCheckboxPrivate *priv =
+      CHEESE_PREFS_SOURCE_CHECKBOX_GET_PRIVATE (self);
+  gboolean value = gtk_toggle_button_get_active (
+      GTK_TOGGLE_BUTTON (checkbutton));
+
+  cheese_camera_set_boolean_property (priv->camera, priv->property_name, value);
+
+  g_object_set (CHEESE_PREFS_WIDGET (self)->gconf,
+                priv->gconf_key, value, NULL);
+
+  cheese_prefs_widget_notify_changed (CHEESE_PREFS_WIDGET (self));
+}
+
+static void
+cheese_prefs_source_checkbox_synchronize (CheesePrefsWidget *prefs_widget)
+{
+  CheesePrefsSourceCheckbox *self =
+      CHEESE_PREFS_SOURCE_CHECKBOX (prefs_widget);
+  CheesePrefsSourceCheckboxPrivate *priv =
+      CHEESE_PREFS_SOURCE_CHECKBOX_GET_PRIVATE (self);
+
+  GtkWidget *checkbox;
+  gboolean stored_value;
+
+  g_object_get (prefs_widget, "widget", &checkbox, NULL);
+
+  /* Disconnect to prevent a whole bunch of changed notifications */
+  g_signal_handlers_disconnect_by_func (checkbox,
+                                        cheese_prefs_source_checkbox_toggled,
+                                        prefs_widget);
+
+  g_object_get (CHEESE_PREFS_WIDGET (self)->gconf,
+                priv->gconf_key, &stored_value, NULL);
+  gtk_toggle_button_set_active (GTK_TOGGLE_BUTTON (checkbox), stored_value);
+
+  g_signal_connect (G_OBJECT (checkbox), "toggled",
+                    G_CALLBACK (cheese_prefs_source_checkbox_toggled),
+                    self);
+}
+
+static void
+cheese_prefs_source_checkbox_set_property (GObject *object,
+                                              guint prop_id,
+                                              const GValue *value,
+                                              GParamSpec *pspec)
+{
+  CheesePrefsSourceCheckboxPrivate *priv =
+      CHEESE_PREFS_SOURCE_CHECKBOX_GET_PRIVATE (object);
+
+  switch (prop_id)
+  {
+    case PROP_PROPERTY:
+      priv->property_name = g_value_dup_string (value);
+      break;
+    case PROP_GCONF_KEY:
+      priv->gconf_key = g_value_dup_string (value);
+      break;
+    case PROP_CAMERA:
+      priv->camera = CHEESE_CAMERA (g_value_get_object (value));
+      break;
+    default:
+      G_OBJECT_WARN_INVALID_PROPERTY_ID (object, prop_id, pspec);
+      break;
+  }
+}
+
+static void
+cheese_prefs_source_checkbox_get_property (GObject *object, guint prop_id,
+                                              GValue *value,
+                                              GParamSpec *pspec)
+{
+  CheesePrefsSourceCheckboxPrivate *priv =
+      CHEESE_PREFS_SOURCE_CHECKBOX_GET_PRIVATE (object);
+
+  g_return_if_fail (CHEESE_IS_PREFS_SOURCE_CHECKBOX (object));
+
+  switch (prop_id)
+  {
+    case PROP_PROPERTY:
+      g_value_set_string (value, priv->property_name);
+      break;
+    case PROP_GCONF_KEY:
+      g_value_set_string (value, priv->gconf_key);
+      break;
+    case PROP_CAMERA:
+      g_value_set_object (value, priv->camera);
+      break;
+    default:
+      G_OBJECT_WARN_INVALID_PROPERTY_ID (object, prop_id, pspec);
+      break;
+  }
+}
+
+static void
+cheese_prefs_source_checkbox_class_init (
+    CheesePrefsSourceCheckboxClass *klass)
+{
+  GObjectClass           *object_class = G_OBJECT_CLASS (klass);
+  CheesePrefsWidgetClass *parent_class = CHEESE_PREFS_WIDGET_CLASS (klass);
+
+  g_type_class_add_private (
+      klass, sizeof (CheesePrefsSourceCheckboxPrivate));
+
+  object_class->finalize     = cheese_prefs_source_checkbox_finalize;
+  object_class->set_property = cheese_prefs_source_checkbox_set_property;
+  object_class->get_property = cheese_prefs_source_checkbox_get_property;
+  parent_class->synchronize  = cheese_prefs_source_checkbox_synchronize;
+
+  g_object_class_install_property (object_class,
+      PROP_PROPERTY,
+      g_param_spec_string ("property",
+                           "",
+                           "Property name in the source",
+                           "",
+                           G_PARAM_READWRITE | G_PARAM_CONSTRUCT_ONLY));
+
+  g_object_class_install_property (object_class,
+      PROP_GCONF_KEY,
+      g_param_spec_string ("gconf_key",
+                           "",
+                           "GConf key for property",
+                           "",
+                           G_PARAM_READWRITE | G_PARAM_CONSTRUCT_ONLY));
+
+  g_object_class_install_property (object_class,
+      PROP_CAMERA,
+      g_param_spec_object ("camera",
+                           "camera",
+                           "Camera object",
+                           CHEESE_TYPE_CAMERA,
+                           G_PARAM_READWRITE | G_PARAM_CONSTRUCT_ONLY));
+}
+
+CheesePrefsSourceCheckbox *
+cheese_prefs_source_checkbox_new (GtkWidget *checkbox,
+                              CheeseCamera *camera,
+                              const gchar *property,
+                              const gchar *gconf_key)
+{
+  CheesePrefsSourceCheckbox *self;
+  CheesePrefsSourceCheckboxPrivate *priv;
+
+  self = g_object_new (CHEESE_TYPE_PREFS_SOURCE_CHECKBOX,
+                       "widget", checkbox,
+                       "camera", camera,
+                       "gconf_key", gconf_key,
+                       NULL);
+
+  priv = CHEESE_PREFS_SOURCE_CHECKBOX_GET_PRIVATE (self);
+
+  return self;
+}
diff --git a/src/cheese-prefs-source-checkbox.h b/src/cheese-prefs-source-checkbox.h
new file mode 100644
index 0000000..8352b0a
--- /dev/null
+++ b/src/cheese-prefs-source-checkbox.h
@@ -0,0 +1,67 @@
+/*
+ * Copyright © 2011 Collabora Ltda
+ *  @author: Luciana Fujii Pontello <luciana.fujii@collabora.co.uk>
+ *
+ * Licensed under the GNU General Public License Version 2
+ *
+ * This program is free software; you can redistribute it and/or modify
+ * it under the terms of the GNU General Public License as published by
+ * the Free Software Foundation; either version 2 of the License, or
+ * (at your option) any later version.
+ *
+ * This program is distributed in the hope that it will be useful,
+ * but WITHOUT ANY WARRANTY; without even the implied warranty of
+ * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
+ * GNU General Public License for more details.
+ *
+ * You should have received a copy of the GNU General Public License
+ * along with this program.  If not, see <http://www.gnu.org/licenses/>.
+ */
+
+#ifndef _CHEESE_PREFS_SOURCE_CHECKBOX_H_
+#define _CHEESE_PREFS_SOURCE_CHECKBOX_H_
+
+#include <glib-object.h>
+#include <cheese-camera.h>
+#include "cheese-prefs-widget.h"
+
+G_BEGIN_DECLS
+
+#define CHEESE_TYPE_PREFS_SOURCE_CHECKBOX \
+  (cheese_prefs_source_checkbox_get_type ())
+#define CHEESE_PREFS_SOURCE_CHECKBOX(obj) \
+  (G_TYPE_CHECK_INSTANCE_CAST ((obj), CHEESE_TYPE_PREFS_SOURCE_CHECKBOX, \
+                               CheesePrefsSourceCheckbox))
+#define CHEESE_PREFS_SOURCE_CHECKBOX_CLASS(klass) \
+  (G_TYPE_CHECK_CLASS_CAST ((klass), CHEESE_TYPE_PREFS_SOURCE_CHECKBOX, \
+                            CheesePrefsSourceCheckboxClass))
+#define CHEESE_IS_PREFS_SOURCE_CHECKBOX(obj) \
+  (G_TYPE_CHECK_INSTANCE_TYPE ((obj), CHEESE_TYPE_PREFS_SOURCE_CHECKBOX))
+#define CHEESE_IS_PREFS_SOURCE_CHECKBOX_CLASS(klass) \
+  (G_TYPE_CHECK_CLASS_TYPE ((klass), CHEESE_TYPE_PREFS_SOURCE_CHECKBOX))
+#define CHEESE_PREFS_SOURCE_CHECKBOX_GET_CLASS(obj) \
+  (G_TYPE_INSTANCE_GET_CLASS ((obj), CHEESE_TYPE_PREFS_SOURCE_CHECKBOX, \
+                              CheesePrefsSourceCheckboxClass))
+
+typedef struct _CheesePrefsSourceCheckboxClass \
+          CheesePrefsSourceCheckboxClass;
+typedef struct _CheesePrefsSourceCheckbox CheesePrefsSourceCheckbox;
+
+struct _CheesePrefsSourceCheckboxClass
+{
+  CheesePrefsWidgetClass parent_class;
+};
+
+struct _CheesePrefsSourceCheckbox
+{
+  CheesePrefsWidget parent_instance;
+};
+
+GType cheese_prefs_source_checkbox_get_type (void) G_GNUC_CONST;
+CheesePrefsSourceCheckbox *cheese_prefs_source_checkbox_new (
+    GtkWidget *checkbox,
+    CheeseCamera *camera,
+    const gchar *property,
+    const gchar  *gconf_key);
+
+#endif /* _CHEESE_PREFS_SOURCE_CHECKBOX_H_ */
-- 
1.7.1

