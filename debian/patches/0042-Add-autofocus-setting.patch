From 0fae254832a1a8cd4f3597760ef545d40e89de69 Mon Sep 17 00:00:00 2001
From: Luciana Fujii Pontello <luciana.fujii@collabora.co.uk>
Date: Mon, 6 Jun 2011 20:19:08 -0300
Subject: [PATCH 42/96] Add autofocus setting

Created cheese-prefs-autofocus-checkbox type and added it to Cheese
preferences dialog.

Added autofocus property to cheese-gconf.
---
 libcheese/cheese-camera.c             |   14 ++
 libcheese/cheese-camera.h             |    2 +
 libcheese/cheese-gconf.c              |   21 ++++
 libcheese/cheese-gconf.h              |    3 +-
 libcheese/cheese-widget.c             |    3 +
 src/Makefile.am                       |    2 +
 src/cheese-prefs-autofocus-checkbox.c |  211 +++++++++++++++++++++++++++++++++
 src/cheese-prefs-autofocus-checkbox.h |   66 ++++++++++
 src/cheese-prefs-dialog.c             |   11 ++
 9 files changed, 332 insertions(+), 1 deletions(-)
 create mode 100644 src/cheese-prefs-autofocus-checkbox.c
 create mode 100644 src/cheese-prefs-autofocus-checkbox.h

diff --git a/libcheese/cheese-camera.c b/libcheese/cheese-camera.c
index ce11f5a..9321652 100644
--- a/libcheese/cheese-camera.c
+++ b/libcheese/cheese-camera.c
@@ -1346,3 +1346,17 @@ cheese_camera_set_float_property (CheeseCamera *camera,
   g_object_set (priv->photography, property, value, NULL);
   return TRUE;
 }
+
+gboolean
+cheese_camera_set_autofocus (CheeseCamera *camera, gboolean autofocus)
+{
+  CheeseCameraPrivate *priv;
+  g_return_val_if_fail (CHEESE_IS_CAMERA (camera), FALSE);
+  priv = CHEESE_CAMERA_GET_PRIVATE (camera);
+
+  if (priv->photography == NULL)
+    return FALSE;
+  gst_photography_set_autofocus (GST_PHOTOGRAPHY (priv->photography),
+                                 autofocus);
+  return TRUE;
+}
diff --git a/libcheese/cheese-camera.h b/libcheese/cheese-camera.h
index 1818e5a..5478ceb 100644
--- a/libcheese/cheese-camera.h
+++ b/libcheese/cheese-camera.h
@@ -115,6 +115,8 @@ gboolean cheese_camera_set_balance_property (CheeseCamera *camera, const gchar *
 gboolean cheese_camera_set_float_property (CheeseCamera *camera,
                                            const gchar *property,
                                            gfloat value);
+gboolean cheese_camera_set_autofocus (CheeseCamera *camera, gboolean autofocus);
+
 G_END_DECLS
 
 #endif /* __CHEESE_CAMERA_H__ */
diff --git a/libcheese/cheese-gconf.c b/libcheese/cheese-gconf.c
index de82e68..ef965ed 100644
--- a/libcheese/cheese-gconf.c
+++ b/libcheese/cheese-gconf.c
@@ -1,5 +1,7 @@
 /*
  * Copyright © 2007-2009 daniel g. siegel <dgsiegel@gnome.org>
+ * Copyright © 2011 Collabora Ltda
+ *  @author: Luciana Fujii Pontello <luciana.fujii@collabora.co.uk>
  *
  * Licensed under the GNU General Public License Version 2
  *
@@ -232,6 +234,12 @@ cheese_gconf_get_property (GObject *object, guint prop_id, GValue *value,
                                   CHEESE_GCONF_PREFIX "/evcompensation",
                                   NULL));
       break;
+    case GCONF_PROP_AUTOFOCUS:
+      g_value_set_boolean (value,
+          gconf_client_get_bool (priv->client,
+                                 CHEESE_GCONF_PREFIX "/autofocus",
+                                 NULL));
+      break;
     default:
       G_OBJECT_WARN_INVALID_PROPERTY_ID (object, prop_id, pspec);
       break;
@@ -427,6 +435,12 @@ cheese_gconf_set_property (GObject *object, guint prop_id, const GValue *value,
                               g_value_get_float (value),
                               NULL);
       break;
+    case GCONF_PROP_AUTOFOCUS:
+      gconf_client_set_bool (priv->client,
+                             CHEESE_GCONF_PREFIX "/autofocus",
+                             g_value_get_boolean (value),
+                             NULL);
+      break;
     default:
       G_OBJECT_WARN_INVALID_PROPERTY_ID (object, prop_id, pspec);
       break;
@@ -649,6 +663,13 @@ cheese_gconf_class_init (CheeseGConfClass *klass)
                                      0,
                                      G_PARAM_READWRITE));
 
+  g_object_class_install_property (object_class, GCONF_PROP_AUTOFOCUS,
+                                   g_param_spec_boolean (
+                                   "gconf_prop_autofocus",
+                                   NULL,
+                                   NULL,
+                                   TRUE,
+                                   G_PARAM_READWRITE));
   g_type_class_add_private (klass, sizeof (CheeseGConfPrivate));
 }
 
diff --git a/libcheese/cheese-gconf.h b/libcheese/cheese-gconf.h
index 12df96b..56d4c9f 100644
--- a/libcheese/cheese-gconf.h
+++ b/libcheese/cheese-gconf.h
@@ -64,7 +64,8 @@ enum
   GCONF_PROP_ISO,
   GCONF_PROP_SHARPNESS,
   GCONF_PROP_ZOOM,
-  GCONF_PROP_EV_COMPENSATION
+  GCONF_PROP_EV_COMPENSATION,
+  GCONF_PROP_AUTOFOCUS
 };
 
 GType        cheese_gconf_get_type (void);
diff --git a/libcheese/cheese-widget.c b/libcheese/cheese-widget.c
index dfa7d56..01cbbbb 100644
--- a/libcheese/cheese-widget.c
+++ b/libcheese/cheese-widget.c
@@ -307,6 +307,7 @@ setup_camera (CheeseWidget *widget)
   int                  flicker_mode;
   int                  scene_mode;
   guint                iso_speed;
+  gboolean             autofocus;
 
   g_object_get (priv->gconf,
                 "gconf_prop_x_resolution", &x_resolution,
@@ -318,6 +319,7 @@ setup_camera (CheeseWidget *widget)
                 "gconf_prop_sharpness", &sharpness,
                 "gconf_prop_zoom", &zoom,
                 "gconf_prop_ev-compensation", &ev_compensation,
+                "gconf_prop_autofocus", &autofocus,
                 "gconf_prop_white-balance-mode", &white_balance_mode,
                 "gconf_prop_flicker-mode", &flicker_mode,
                 "gconf_prop_scene-mode", &scene_mode,
@@ -354,6 +356,7 @@ setup_camera (CheeseWidget *widget)
     cheese_camera_set_balance_property (priv->webcam, "zoom", zoom);
     cheese_camera_set_float_property (priv->webcam,
         "ev-compensation", ev_compensation);
+    cheese_camera_set_autofocus (priv->webcam, autofocus);
     cheese_camera_set_photography_property (priv->webcam,
         "white-balance-mode", white_balance_mode);
     cheese_camera_set_photography_property (priv->webcam,
diff --git a/src/Makefile.am b/src/Makefile.am
index 98d8948..f309acc 100644
--- a/src/Makefile.am
+++ b/src/Makefile.am
@@ -45,6 +45,8 @@ cheese_SOURCES = \
 	cheese-prefs-balance-scale.h \
 	cheese-prefs-float-scale.c \
 	cheese-prefs-float-scale.h \
+	cheese-prefs-autofocus-checkbox.c \
+	cheese-prefs-autofocus-checkbox.h \
 	cheese-prefs-dialog.c \
 	cheese-prefs-dialog.h \
 	cheese-prefs-photography-combo.c \
diff --git a/src/cheese-prefs-autofocus-checkbox.c b/src/cheese-prefs-autofocus-checkbox.c
new file mode 100644
index 0000000..9208251
--- /dev/null
+++ b/src/cheese-prefs-autofocus-checkbox.c
@@ -0,0 +1,211 @@
+/*
+ * Copyright © 2011 Collabora Ltda
+ *  @author: Luciana Fujii Pontello <luciana.fujii@collabora.co.uk>
+ *
+ * Licensed under the GNU General Public License Version 2
+ *
+ * This program is free software; you can redistribute it and/or modify
+ * it under the terms of the GNU General Public License as published by
+ * the Free Software Foundation; either version 2 of the License, or
+ * (at your option) any later version.
+ *
+ * This program is distributed in the hope that it will be useful,
+ * but WITHOUT ANY WARRANTY; without even the implied warranty of
+ * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
+ * GNU General Public License for more details.
+ *
+ * You should have received a copy of the GNU General Public License
+ * along with this program.  If not, see <http://www.gnu.org/licenses/>.
+ */
+
+#include <string.h>
+#include <glib.h>
+
+#include <cheese-camera.h>
+#include "cheese-prefs-widget.h"
+#include "cheese-prefs-autofocus-checkbox.h"
+
+enum
+{
+  PROP_0,
+  PROP_GCONF_KEY,
+  PROP_CAMERA
+};
+
+typedef struct CheesePrefsAutofocusCheckboxPrivate
+{
+  CheeseCamera *camera;
+  gchar *gconf_key;
+} CheesePrefsAutofocusCheckboxPrivate;
+
+#define CHEESE_PREFS_AUTOFOCUS_CHECKBOX_GET_PRIVATE(o)                     \
+  (G_TYPE_INSTANCE_GET_PRIVATE ((o), CHEESE_TYPE_PREFS_AUTOFOCUS_CHECKBOX, \
+                                CheesePrefsAutofocusCheckboxPrivate))
+
+G_DEFINE_TYPE (CheesePrefsAutofocusCheckbox, cheese_prefs_autofocus_checkbox,
+               CHEESE_TYPE_PREFS_WIDGET);
+
+static void
+cheese_prefs_autofocus_checkbox_init (CheesePrefsAutofocusCheckbox *self)
+{
+  CheesePrefsAutofocusCheckboxPrivate *priv =
+      CHEESE_PREFS_AUTOFOCUS_CHECKBOX_GET_PRIVATE (self);
+
+  priv->gconf_key = NULL;
+}
+
+static void
+cheese_prefs_autofocus_checkbox_finalize (GObject *object)
+{
+  CheesePrefsAutofocusCheckbox *self = CHEESE_PREFS_AUTOFOCUS_CHECKBOX (object);
+  CheesePrefsAutofocusCheckboxPrivate *priv =
+      CHEESE_PREFS_AUTOFOCUS_CHECKBOX_GET_PRIVATE (self);
+
+  g_free (priv->gconf_key);
+
+  G_OBJECT_CLASS (
+      cheese_prefs_autofocus_checkbox_parent_class)->finalize (object);
+}
+
+static void
+cheese_prefs_autofocus_checkbox_toggled (GtkCheckButton *checkbutton,
+    CheesePrefsAutofocusCheckbox *self)
+{
+  CheesePrefsAutofocusCheckboxPrivate *priv =
+      CHEESE_PREFS_AUTOFOCUS_CHECKBOX_GET_PRIVATE (self);
+  gboolean value = gtk_toggle_button_get_active (
+      GTK_TOGGLE_BUTTON (checkbutton));
+
+  cheese_camera_set_autofocus (priv->camera, value);
+
+  g_object_set (CHEESE_PREFS_WIDGET (self)->gconf,
+                priv->gconf_key, value, NULL);
+
+  cheese_prefs_widget_notify_changed (CHEESE_PREFS_WIDGET (self));
+}
+
+static void
+cheese_prefs_autofocus_checkbox_synchronize (CheesePrefsWidget *prefs_widget)
+{
+  CheesePrefsAutofocusCheckbox *self =
+      CHEESE_PREFS_AUTOFOCUS_CHECKBOX (prefs_widget);
+  CheesePrefsAutofocusCheckboxPrivate *priv =
+      CHEESE_PREFS_AUTOFOCUS_CHECKBOX_GET_PRIVATE (self);
+
+  GtkWidget *checkbox;
+  gboolean stored_value;
+
+  g_object_get (prefs_widget, "widget", &checkbox, NULL);
+
+  /* Disconnect to prevent a whole bunch of changed notifications */
+  g_signal_handlers_disconnect_by_func (checkbox,
+                                        cheese_prefs_autofocus_checkbox_toggled,
+                                        prefs_widget);
+
+  g_object_get (CHEESE_PREFS_WIDGET (self)->gconf,
+                priv->gconf_key, &stored_value, NULL);
+  gtk_toggle_button_set_active (GTK_TOGGLE_BUTTON (checkbox), stored_value);
+
+  g_signal_connect (G_OBJECT (checkbox), "toggled",
+                    G_CALLBACK (cheese_prefs_autofocus_checkbox_toggled),
+                    self);
+}
+
+static void
+cheese_prefs_autofocus_checkbox_set_property (GObject *object,
+                                              guint prop_id,
+                                              const GValue *value,
+                                              GParamSpec *pspec)
+{
+  CheesePrefsAutofocusCheckboxPrivate *priv =
+      CHEESE_PREFS_AUTOFOCUS_CHECKBOX_GET_PRIVATE (object);
+
+  switch (prop_id)
+  {
+    case PROP_GCONF_KEY:
+      priv->gconf_key = g_value_dup_string (value);
+      break;
+    case PROP_CAMERA:
+      priv->camera = CHEESE_CAMERA (g_value_get_object (value));
+      break;
+    default:
+      G_OBJECT_WARN_INVALID_PROPERTY_ID (object, prop_id, pspec);
+      break;
+  }
+}
+
+static void
+cheese_prefs_autofocus_checkbox_get_property (GObject *object,
+                                              guint prop_id,
+                                              GValue *value,
+                                              GParamSpec *pspec)
+{
+  CheesePrefsAutofocusCheckboxPrivate *priv =
+      CHEESE_PREFS_AUTOFOCUS_CHECKBOX_GET_PRIVATE (object);
+
+  g_return_if_fail (CHEESE_IS_PREFS_AUTOFOCUS_CHECKBOX (object));
+
+  switch (prop_id)
+  {
+    case PROP_GCONF_KEY:
+      g_value_set_string (value, priv->gconf_key);
+      break;
+    case PROP_CAMERA:
+      g_value_set_object (value, priv->camera);
+      break;
+    default:
+      G_OBJECT_WARN_INVALID_PROPERTY_ID (object, prop_id, pspec);
+      break;
+  }
+}
+
+static void
+cheese_prefs_autofocus_checkbox_class_init (
+    CheesePrefsAutofocusCheckboxClass *klass)
+{
+  GObjectClass           *object_class = G_OBJECT_CLASS (klass);
+  CheesePrefsWidgetClass *parent_class = CHEESE_PREFS_WIDGET_CLASS (klass);
+
+  g_type_class_add_private (
+      klass, sizeof (CheesePrefsAutofocusCheckboxPrivate));
+
+  object_class->finalize     = cheese_prefs_autofocus_checkbox_finalize;
+  object_class->set_property = cheese_prefs_autofocus_checkbox_set_property;
+  object_class->get_property = cheese_prefs_autofocus_checkbox_get_property;
+  parent_class->synchronize  = cheese_prefs_autofocus_checkbox_synchronize;
+
+  g_object_class_install_property (object_class,
+      PROP_GCONF_KEY,
+      g_param_spec_string ("gconf_key",
+                           "",
+                           "GConf key for property",
+                           "",
+                           G_PARAM_READWRITE | G_PARAM_CONSTRUCT_ONLY));
+
+  g_object_class_install_property (object_class,
+      PROP_CAMERA,
+      g_param_spec_object ("camera",
+                           "camera",
+                           "Camera object",
+                           CHEESE_TYPE_CAMERA,
+                           G_PARAM_READWRITE | G_PARAM_CONSTRUCT_ONLY));
+}
+
+CheesePrefsAutofocusCheckbox *
+cheese_prefs_autofocus_checkbox_new (GtkWidget *checkbox,
+                              CheeseCamera *camera,
+                              const gchar  *gconf_key)
+{
+  CheesePrefsAutofocusCheckbox *self;
+  CheesePrefsAutofocusCheckboxPrivate *priv;
+
+  self = g_object_new (CHEESE_TYPE_PREFS_AUTOFOCUS_CHECKBOX,
+                       "widget", checkbox,
+                       "camera", camera,
+                       "gconf_key", gconf_key,
+                       NULL);
+
+  priv = CHEESE_PREFS_AUTOFOCUS_CHECKBOX_GET_PRIVATE (self);
+
+  return self;
+}
diff --git a/src/cheese-prefs-autofocus-checkbox.h b/src/cheese-prefs-autofocus-checkbox.h
new file mode 100644
index 0000000..1eb8e93
--- /dev/null
+++ b/src/cheese-prefs-autofocus-checkbox.h
@@ -0,0 +1,66 @@
+/*
+ * Copyright © 2011 Collabora Ltda
+ *  @author: Luciana Fujii Pontello <luciana.fujii@collabora.co.uk>
+ *
+ * Licensed under the GNU General Public License Version 2
+ *
+ * This program is free software; you can redistribute it and/or modify
+ * it under the terms of the GNU General Public License as published by
+ * the Free Software Foundation; either version 2 of the License, or
+ * (at your option) any later version.
+ *
+ * This program is distributed in the hope that it will be useful,
+ * but WITHOUT ANY WARRANTY; without even the implied warranty of
+ * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
+ * GNU General Public License for more details.
+ *
+ * You should have received a copy of the GNU General Public License
+ * along with this program.  If not, see <http://www.gnu.org/licenses/>.
+ */
+
+#ifndef _CHEESE_PREFS_AUTOFOCUS_CHECKBOX_H_
+#define _CHEESE_PREFS_AUTOFOCUS_CHECKBOX_H_
+
+#include <glib-object.h>
+#include <cheese-camera.h>
+#include "cheese-prefs-widget.h"
+
+G_BEGIN_DECLS
+
+#define CHEESE_TYPE_PREFS_AUTOFOCUS_CHECKBOX \
+  (cheese_prefs_autofocus_checkbox_get_type ())
+#define CHEESE_PREFS_AUTOFOCUS_CHECKBOX(obj) \
+  (G_TYPE_CHECK_INSTANCE_CAST ((obj), CHEESE_TYPE_PREFS_AUTOFOCUS_CHECKBOX, \
+                               CheesePrefsAutofocusCheckbox))
+#define CHEESE_PREFS_AUTOFOCUS_CHECKBOX_CLASS(klass) \
+  (G_TYPE_CHECK_CLASS_CAST ((klass), CHEESE_TYPE_PREFS_AUTOFOCUS_CHECKBOX, \
+                            CheesePrefsAutofocusCheckboxClass))
+#define CHEESE_IS_PREFS_AUTOFOCUS_CHECKBOX(obj) \
+  (G_TYPE_CHECK_INSTANCE_TYPE ((obj), CHEESE_TYPE_PREFS_AUTOFOCUS_CHECKBOX))
+#define CHEESE_IS_PREFS_AUTOFOCUS_CHECKBOX_CLASS(klass) \
+  (G_TYPE_CHECK_CLASS_TYPE ((klass), CHEESE_TYPE_PREFS_AUTOFOCUS_CHECKBOX))
+#define CHEESE_PREFS_AUTOFOCUS_CHECKBOX_GET_CLASS(obj) \
+  (G_TYPE_INSTANCE_GET_CLASS ((obj), CHEESE_TYPE_PREFS_AUTOFOCUS_CHECKBOX, \
+                              CheesePrefsAutofocusCheckboxClass))
+
+typedef struct _CheesePrefsAutofocusCheckboxClass \
+          CheesePrefsAutofocusCheckboxClass;
+typedef struct _CheesePrefsAutofocusCheckbox CheesePrefsAutofocusCheckbox;
+
+struct _CheesePrefsAutofocusCheckboxClass
+{
+  CheesePrefsWidgetClass parent_class;
+};
+
+struct _CheesePrefsAutofocusCheckbox
+{
+  CheesePrefsWidget parent_instance;
+};
+
+GType cheese_prefs_autofocus_checkbox_get_type (void) G_GNUC_CONST;
+CheesePrefsAutofocusCheckbox *cheese_prefs_autofocus_checkbox_new (
+    GtkWidget *scale,
+    CheeseCamera *camera,
+    const gchar  *balance_key);
+
+#endif /* _CHEESE_PREFS_AUTOFOCUS_CHECKBOX_H_ */
diff --git a/src/cheese-prefs-dialog.c b/src/cheese-prefs-dialog.c
index f2a7205..7053259 100644
--- a/src/cheese-prefs-dialog.c
+++ b/src/cheese-prefs-dialog.c
@@ -20,6 +20,7 @@
 
 #include "cheese-prefs-dialog.h"
 #include "cheese-prefs-photography-combo.h"
+#include "cheese-prefs-autofocus-checkbox.h"
 
 typedef struct
 {
@@ -36,6 +37,7 @@ typedef struct
   GtkWidget *sharpness_scale;
   GtkWidget *zoom_scale;
   GtkWidget *ev_compensation_scale;
+  GtkWidget *autofocus_checkbutton;
   GtkWidget *burst_repeat;
   GtkWidget *burst_delay;
 
@@ -134,6 +136,8 @@ cheese_prefs_dialog_create_dialog (CheesePrefsDialog *prefs_dialog)
     GTK_WIDGET (gtk_builder_get_object (builder, "zoom_scale"));
   prefs_dialog->ev_compensation_scale =
     GTK_WIDGET (gtk_builder_get_object (builder, "ev_scale"));
+  prefs_dialog->autofocus_checkbutton =
+    GTK_WIDGET (gtk_builder_get_object (builder, "auto_focus_button"));
   prefs_dialog->burst_repeat = GTK_WIDGET (gtk_builder_get_object (builder, "burst_repeat"));
 
   prefs_dialog->burst_delay = GTK_WIDGET (gtk_builder_get_object (builder, "burst_delay"));
@@ -193,6 +197,7 @@ cheese_prefs_dialog_setup_widgets (CheesePrefsDialog *prefs_dialog)
   CheesePrefsWidget *sharpness_widget;
   CheesePrefsWidget *zoom_widget;
   CheesePrefsWidget *ev_widget;
+  CheesePrefsWidget *autofocus_widget;
   CheesePrefsWidget *burst_delay_widget;
   CheesePrefsWidget *burst_repeat_widget;
 
@@ -279,6 +284,12 @@ cheese_prefs_dialog_setup_widgets (CheesePrefsDialog *prefs_dialog)
                                     "gconf_prop_ev-compensation"));
   cheese_prefs_dialog_widgets_add (prefs_dialog->widgets, ev_widget);
 
+  autofocus_widget = CHEESE_PREFS_WIDGET (
+      cheese_prefs_autofocus_checkbox_new (prefs_dialog->autofocus_checkbutton,
+                                           prefs_dialog->camera,
+                                           "gconf_prop_autofocus"));
+  cheese_prefs_dialog_widgets_add (prefs_dialog->widgets, autofocus_widget);
+
   burst_repeat_widget = CHEESE_PREFS_WIDGET (cheese_prefs_burst_spinbox_new (prefs_dialog->burst_repeat,
                                                                              "gconf_prop_burst_repeat"));
 
-- 
1.7.1

