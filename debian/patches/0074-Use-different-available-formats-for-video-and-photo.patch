From 1bd14619a31e11d40ce63e45abc161d0d2ad6e41 Mon Sep 17 00:00:00 2001
From: Luciana Fujii Pontello <luciana.fujii@collabora.co.uk>
Date: Sat, 30 Jul 2011 18:43:31 -0300
Subject: [PATCH 74/96] Use different available formats for video and photo

OmxCamera has different resolution capabilities for video and photo.
Because of that, for omx_camera we use two different formats list and
display different format options for photo and video.
---
 libcheese/cheese-camera-device.c    |  112 +++++++++++++++++++++++++----------
 libcheese/cheese-camera-device.h    |    3 +-
 libcheese/cheese-camera.c           |   17 +++++-
 libcheese/cheese-camera.h           |    4 +-
 src/cheese-prefs-resolution-combo.c |    6 ++-
 5 files changed, 107 insertions(+), 35 deletions(-)

diff --git a/libcheese/cheese-camera-device.c b/libcheese/cheese-camera-device.c
index 5b128ef..1aac75d 100644
--- a/libcheese/cheese-camera-device.c
+++ b/libcheese/cheese-camera-device.c
@@ -3,6 +3,8 @@
  * Copyright © 2007,2008 Jaap Haitsma <jaap@haitsma.org>
  * Copyright © 2007-2009 daniel g. siegel <dgsiegel@gnome.org>
  * Copyright © 2008 Ryan Zeigler <zeiglerr@gmail.com>
+ * Copyright © 2011 Collabora Ltda
+ *  @author: Luciana Fujii Pontello <luciana.fujii@collabora.com>
  *
  * Licensed under the GNU General Public License Version 2
  *
@@ -62,27 +64,38 @@ static gchar *supported_formats[] = {
   NULL
 };
 
-static gint omx_formats[][2] = {
+static gint omx_video_formats[][2] = {
   /* screen 4:3 */
   {320, 240},
   {640, 480},
   {800, 600},
   {1024, 768},
-  {1280, 960}, //
-  {1600, 1200},
-  {2048, 1536},
-  {2592, 1952}, //
+  {1280, 960},
 
   /* screen 16:9 */
   {852, 480},
   {1280, 720},
   {1366, 768},
-  {1920, 1080},
-  {2560, 1600}, //16:10
-  {3200, 2048}, //1.56:1
-  {3840, 2400}, //16:10
 
-  /* Photo */
+  {0,0}
+};
+
+static gint omx_photo_formats[][2] = {
+  /* screen 4:3 */
+  {320, 240},
+  {640, 480},
+  {800, 600},
+  {1024, 768},
+  {1280, 960}, //
+  {1600, 1200},
+  {2048, 1536},
+  {2592, 1952}, //
+
+  /* Photo widescreen*/
+  {640, 384},
+  {1280, 768},
+  {2048, 1216},
+  {2592, 1552},
   {3648, 2736},
   {4000, 3000},
   {0,0}
@@ -109,7 +122,8 @@ typedef struct
   gchar *name;
   gint api;
   GstCaps *caps;
-  GList *formats;
+  GList *video_formats;
+  GList *photo_formats;
 
   GError *construct_error;
 } CheeseCameraDevicePrivate;
@@ -196,13 +210,13 @@ cheese_webcam_device_filter_caps (CheeseCameraDevice *device, const GstCaps *cap
 }
 
 static void
-cheese_camera_device_add_format (CheeseCameraDevice *device, CheeseVideoFormat *format)
+cheese_camera_device_add_format (CheeseCameraDevice *device,
+                                 GList **formats,
+                                 CheeseVideoFormat *format)
 {
-  CheeseCameraDevicePrivate *priv =
-    CHEESE_CAMERA_DEVICE_GET_PRIVATE (device);
   GList *l;
 
-  for (l = priv->formats; l != NULL; l = l->next)
+  for (l = *formats; l != NULL; l = l->next)
   {
     CheeseVideoFormat *item = l->data;
     if ((item != NULL) &&
@@ -216,7 +230,7 @@ cheese_camera_device_add_format (CheeseCameraDevice *device, CheeseVideoFormat *
 
   GST_INFO ("%dx%d", format->width, format->height);
 
-  priv->formats = g_list_append (priv->formats, format);
+  *formats = g_list_prepend (*formats, format);
 }
 
 static void
@@ -227,10 +241,15 @@ free_format_list (CheeseCameraDevice *device)
 
   GList *l;
 
-  for (l = priv->formats; l != NULL; l = l->next)
+  for (l = priv->video_formats; l != NULL; l = l->next)
     g_free (l->data);
-  g_list_free (priv->formats);
-  priv->formats = NULL;
+  g_list_free (priv->video_formats);
+  priv->video_formats = NULL;
+
+  for (l = priv->photo_formats; l != NULL; l = l->next)
+    g_free (l->data);
+  g_list_free (priv->photo_formats);
+  priv->photo_formats = NULL;
 }
 
 static void
@@ -261,13 +280,25 @@ cheese_webcam_device_update_format_table (CheeseCameraDevice *device)
       max_width  = gst_value_get_int_range_max (width);
       max_height = gst_value_get_int_range_max (height);
 
-      for (j=0; omx_formats[j][0] != 0; j++)
+      for (j=0; omx_video_formats[j][0] != 0; j++)
       {
         CheeseVideoFormat *format = g_new0 (CheeseVideoFormat, 1);
-        format->width = omx_formats[j][0];
-        format->height = omx_formats[j][1];
+        format->width = omx_video_formats[j][0];
+        format->height = omx_video_formats[j][1];
         if (format->width <= max_width && format->height <= max_height)
-          cheese_camera_device_add_format (device, format);
+          cheese_camera_device_add_format (device, &(priv->video_formats),
+                                           format);
+        else
+          g_free (format);
+      }
+      for (j=0; omx_photo_formats[j][0] != 0; j++)
+      {
+        CheeseVideoFormat *format = g_new0 (CheeseVideoFormat, 1);
+        format->width = omx_photo_formats[j][0];
+        format->height = omx_photo_formats[j][1];
+        if (format->width <= max_width && format->height <= max_height)
+          cheese_camera_device_add_format (device, &(priv->photo_formats),
+                                           format);
         else
           g_free (format);
       }
@@ -279,7 +310,9 @@ cheese_webcam_device_update_format_table (CheeseCameraDevice *device)
 
       gst_structure_get_int (structure, "width", &(format->width));
       gst_structure_get_int (structure, "height", &(format->height));
-      cheese_camera_device_add_format (device, format);
+      cheese_camera_device_add_format (device, &(priv->video_formats), format);
+      cheese_camera_device_add_format (device, &(priv->photo_formats),
+                                       g_object_ref (format));
     }
     else if (GST_VALUE_HOLDS_INT_RANGE (width))
     {
@@ -303,7 +336,10 @@ cheese_webcam_device_update_format_table (CheeseCameraDevice *device)
         format->width  = cur_width;
         format->height = cur_height;
 
-        cheese_camera_device_add_format (device, format);
+        cheese_camera_device_add_format (device, &(priv->video_formats),
+                                         format);
+        cheese_camera_device_add_format (device, &(priv->photo_formats),
+                                         g_object_ref (format));
 
         cur_width  *= 2;
         cur_height *= 2;
@@ -318,7 +354,10 @@ cheese_webcam_device_update_format_table (CheeseCameraDevice *device)
         format->width  = cur_width;
         format->height = cur_height;
 
-        cheese_camera_device_add_format (device, format);
+        cheese_camera_device_add_format (device, &(priv->video_formats),
+                                         format);
+        cheese_camera_device_add_format (device, &(priv->photo_formats),
+                                         g_object_ref (format));
 
         cur_width  /= 2;
         cur_height /= 2;
@@ -568,7 +607,8 @@ cheese_camera_device_init (CheeseCameraDevice *device)
   priv->name   = g_strdup (_("Unknown device"));
   priv->caps   = gst_caps_new_empty ();
 
-  priv->formats = NULL;
+  priv->video_formats = NULL;
+  priv->photo_formats = NULL;
 
   priv->construct_error = NULL;
 }
@@ -622,12 +662,21 @@ cheese_camera_device_new (const gchar *device_id,
 /* public methods */
 
 GList *
-cheese_camera_device_get_format_list (CheeseCameraDevice *device)
+cheese_camera_device_get_video_format_list (CheeseCameraDevice *device)
+{
+  CheeseCameraDevicePrivate *priv =
+    CHEESE_CAMERA_DEVICE_GET_PRIVATE (device);
+
+  return g_list_sort (g_list_copy (priv->video_formats), compare_formats);
+}
+
+GList *
+cheese_camera_device_get_photo_format_list (CheeseCameraDevice *device)
 {
   CheeseCameraDevicePrivate *priv =
     CHEESE_CAMERA_DEVICE_GET_PRIVATE (device);
 
-  return g_list_sort (g_list_copy (priv->formats), compare_formats);
+  return g_list_sort (g_list_copy (priv->photo_formats), compare_formats);
 }
 
 const gchar *
@@ -669,8 +718,9 @@ cheese_camera_device_get_device_file (CheeseCameraDevice *device)
 CheeseVideoFormat *
 cheese_camera_device_get_best_format (CheeseCameraDevice *device)
 {
-  CheeseVideoFormat *format = g_boxed_copy (CHEESE_TYPE_VIDEO_FORMAT,
-                                            cheese_camera_device_get_format_list (device)->data);
+  CheeseVideoFormat *format =
+      g_boxed_copy (CHEESE_TYPE_VIDEO_FORMAT,
+                    cheese_camera_device_get_video_format_list (device)->data);
 
   GST_INFO ("%dx%d", format->width, format->height);
   return format;
diff --git a/libcheese/cheese-camera-device.h b/libcheese/cheese-camera-device.h
index 56e2773..c43048a 100644
--- a/libcheese/cheese-camera-device.h
+++ b/libcheese/cheese-camera-device.h
@@ -69,7 +69,8 @@ CheeseCameraDevice *cheese_camera_device_new (const gchar *device_id,
 GstCaps *           cheese_camera_device_get_caps_for_format (CheeseCameraDevice *device,
                                                               CheeseVideoFormat  *format);
 CheeseVideoFormat *cheese_camera_device_get_best_format (CheeseCameraDevice *device);
-GList *            cheese_camera_device_get_format_list (CheeseCameraDevice *device);
+GList* cheese_camera_device_get_video_format_list (CheeseCameraDevice *device);
+GList* cheese_camera_device_get_photo_format_list (CheeseCameraDevice *device);
 
 const gchar *cheese_camera_device_get_name (CheeseCameraDevice *device);
 const gchar *cheese_camera_device_get_src (CheeseCameraDevice *device);
diff --git a/libcheese/cheese-camera.c b/libcheese/cheese-camera.c
index 0e8402f..2cae334 100644
--- a/libcheese/cheese-camera.c
+++ b/libcheese/cheese-camera.c
@@ -1247,7 +1247,22 @@ cheese_camera_get_video_formats (CheeseCamera *camera)
   device = cheese_camera_get_selected_device (camera);
 
   if (device)
-    return cheese_camera_device_get_format_list (device);
+    return cheese_camera_device_get_video_format_list (device);
+  else
+    return NULL;
+}
+
+GList *
+cheese_camera_get_photo_formats (CheeseCamera *camera)
+{
+  CheeseCameraDevice *device;
+
+  g_return_val_if_fail (CHEESE_IS_CAMERA (camera), NULL);
+
+  device = cheese_camera_get_selected_device (camera);
+
+  if (device)
+    return cheese_camera_device_get_photo_format_list (device);
   else
     return NULL;
 }
diff --git a/libcheese/cheese-camera.h b/libcheese/cheese-camera.h
index b5397ae..4a27dc3 100644
--- a/libcheese/cheese-camera.h
+++ b/libcheese/cheese-camera.h
@@ -100,7 +100,9 @@ GPtrArray *              cheese_camera_get_camera_devices (CheeseCamera *camera)
 void                     cheese_camera_set_device_by_dev_file (CheeseCamera *camera, char *file);
 void                     cheese_camera_set_device_by_dev_udi (CheeseCamera *camera, char *udi);
 gboolean                 cheese_camera_switch_camera_device (CheeseCamera *camera);
-GList *                  cheese_camera_get_video_formats (CheeseCamera *camera);
+
+GList* cheese_camera_get_video_formats (CheeseCamera *camera);
+GList* cheese_camera_get_photo_formats (CheeseCamera *camera);
 void cheese_camera_set_video_format (CheeseCamera *camera,
                                      CheeseVideoFormat *format,
                                      CheeseMediaMode mode);
diff --git a/src/cheese-prefs-resolution-combo.c b/src/cheese-prefs-resolution-combo.c
index 93abc8a..2aff1e5 100644
--- a/src/cheese-prefs-resolution-combo.c
+++ b/src/cheese-prefs-resolution-combo.c
@@ -17,6 +17,7 @@
  * along with this program.  If not, see <http://www.gnu.org/licenses/>.
  */
 
+#include <string.h>
 #include "cheese-prefs-resolution-combo.h"
 
 typedef struct
@@ -139,7 +140,10 @@ cheese_prefs_resolution_combo_synchronize (CheesePrefsWidget *prefs_widget)
                 priv->y_resolution_key, &(current_format->height), NULL);
 
   gtk_list_store_clear (priv->list_store);
-  formats = cheese_camera_get_video_formats (priv->camera);
+  if (strstr (priv->x_resolution_key, "video"))
+    formats = cheese_camera_get_video_formats (priv->camera);
+  else
+    formats = cheese_camera_get_photo_formats (priv->camera);
 
   for (l = formats; l != NULL; l = l->next)
   {
-- 
1.7.1

