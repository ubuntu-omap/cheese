From 6e85d1100de73317732d24c53596839c2aa5225d Mon Sep 17 00:00:00 2001
From: Luciana Fujii Pontello <luciana.fujii@collabora.co.uk>
Date: Tue, 31 May 2011 00:30:01 -0300
Subject: [PATCH 29/96] prefs-dialog: Adding widgets in prefs dialog

Adding combo boxes to control white-balance, flicker mode, and scene
mode and adding a scale to control sharpness.

Uses properties "scene-mode", "white-balance-mode" and "flicker-mode",
all GstPhotography properties, instead of using native OmxCamera
properties.
---
 src/cheese-prefs-dialog.c |   47 +++++++++++++++++++++++++++++++++++++++++++-
 1 files changed, 45 insertions(+), 2 deletions(-)

diff --git a/src/cheese-prefs-dialog.c b/src/cheese-prefs-dialog.c
index 979ffc7..485c70d 100644
--- a/src/cheese-prefs-dialog.c
+++ b/src/cheese-prefs-dialog.c
@@ -19,15 +19,21 @@
  */
 
 #include "cheese-prefs-dialog.h"
+#include "cheese-prefs-photography-combo.h"
 
 typedef struct
 {
   GtkWidget *cheese_prefs_dialog;
   GtkWidget *resolution_combo_box;
   GtkWidget *camera_combo_box;
+  GtkWidget *wb_combo_box;
+  GtkWidget *iso_combo_box;
+  GtkWidget *flicker_combo_box;
+  GtkWidget *scene_combo_box;
   GtkWidget *brightness_scale;
   GtkWidget *contrast_scale;
   GtkWidget *saturation_scale;
+  GtkWidget *sharpness_scale;
   GtkWidget *hue_scale;
   GtkWidget *burst_repeat;
   GtkWidget *burst_delay;
@@ -56,6 +62,14 @@ cheese_prefs_dialog_create_dialog (CheesePrefsDialog *prefs_dialog)
 
   prefs_dialog->cheese_prefs_dialog = GTK_WIDGET (gtk_builder_get_object (builder,
                                                                           "cheese_prefs_dialog"));
+  prefs_dialog->wb_combo_box =
+      GTK_WIDGET (gtk_builder_get_object (builder, "white_balance_combo_box"));
+  prefs_dialog->flicker_combo_box =
+      GTK_WIDGET (gtk_builder_get_object (builder, "flicker_combo_box"));
+  prefs_dialog->scene_combo_box =
+      GTK_WIDGET (gtk_builder_get_object (builder, "scene_combo_box"));
+  prefs_dialog->iso_combo_box =
+      GTK_WIDGET (gtk_builder_get_object (builder, "iso_combo_box"));
   prefs_dialog->resolution_combo_box = GTK_WIDGET (gtk_builder_get_object (builder,
                                                                            "resolution_combo_box"));
   prefs_dialog->camera_combo_box = GTK_WIDGET (gtk_builder_get_object (builder,
@@ -66,6 +80,8 @@ cheese_prefs_dialog_create_dialog (CheesePrefsDialog *prefs_dialog)
                                                                      "contrast_scale"));
   prefs_dialog->saturation_scale = GTK_WIDGET (gtk_builder_get_object (builder,
                                                                        "saturation_scale"));
+  prefs_dialog->sharpness_scale =
+    GTK_WIDGET (gtk_builder_get_object (builder, "sharpness_scale"));
   prefs_dialog->hue_scale = GTK_WIDGET (gtk_builder_get_object (builder,
                                                                 "hue_scale"));
   prefs_dialog->burst_repeat = GTK_WIDGET (gtk_builder_get_object (builder, "burst_repeat"));
@@ -117,9 +133,13 @@ cheese_prefs_dialog_setup_widgets (CheesePrefsDialog *prefs_dialog)
 {
   CheesePrefsWidget *resolution_widget;
   CheesePrefsWidget *camera_widget;
+  CheesePrefsWidget *wb_widget;
+  CheesePrefsWidget *flicker_widget;
+  CheesePrefsWidget *scene_widget;
   CheesePrefsWidget *brightness_widget;
   CheesePrefsWidget *contrast_widget;
   CheesePrefsWidget *saturation_widget;
+  CheesePrefsWidget *sharpness_widget;
   CheesePrefsWidget *hue_widget;
   CheesePrefsWidget *burst_delay_widget;
   CheesePrefsWidget *burst_repeat_widget;
@@ -143,6 +163,24 @@ cheese_prefs_dialog_setup_widgets (CheesePrefsDialog *prefs_dialog)
                     prefs_dialog);
   cheese_prefs_dialog_widgets_add (prefs_dialog->widgets, camera_widget);
 
+  wb_widget = CHEESE_PREFS_WIDGET (
+      cheese_prefs_photography_combo_new (prefs_dialog->wb_combo_box,
+                                          prefs_dialog->camera,
+                                          "white-balance-mode"));
+  cheese_prefs_dialog_widgets_add (prefs_dialog->widgets, wb_widget);
+
+  flicker_widget = CHEESE_PREFS_WIDGET (
+      cheese_prefs_photography_combo_new (prefs_dialog->flicker_combo_box,
+                                          prefs_dialog->camera,
+                                          "flicker-mode"));
+  cheese_prefs_dialog_widgets_add (prefs_dialog->widgets, flicker_widget);
+
+  scene_widget = CHEESE_PREFS_WIDGET (
+      cheese_prefs_photography_combo_new (prefs_dialog->scene_combo_box,
+                                          prefs_dialog->camera,
+                                          "scene-mode"));
+  cheese_prefs_dialog_widgets_add (prefs_dialog->widgets, scene_widget);
+
   brightness_widget = CHEESE_PREFS_WIDGET (cheese_prefs_balance_scale_new (prefs_dialog->brightness_scale,
                                                                            prefs_dialog->camera, "brightness",
                                                                            "gconf_prop_brightness"));
@@ -158,13 +196,18 @@ cheese_prefs_dialog_setup_widgets (CheesePrefsDialog *prefs_dialog)
   saturation_widget = CHEESE_PREFS_WIDGET (cheese_prefs_balance_scale_new (prefs_dialog->saturation_scale,
                                                                            prefs_dialog->camera, "saturation",
                                                                            "gconf_prop_saturation"));
-
   cheese_prefs_dialog_widgets_add (prefs_dialog->widgets, saturation_widget);
 
+
+  sharpness_widget = CHEESE_PREFS_WIDGET (
+      cheese_prefs_balance_scale_new (prefs_dialog->sharpness_scale,
+                                      prefs_dialog->camera, "sharpness",
+                                      "gconf_prop_sharpness"));
+  cheese_prefs_dialog_widgets_add (prefs_dialog->widgets, sharpness_widget);
+
   hue_widget = CHEESE_PREFS_WIDGET (cheese_prefs_balance_scale_new (prefs_dialog->hue_scale,
                                                                     prefs_dialog->camera, "hue",
                                                                     "gconf_prop_hue"));
-
   cheese_prefs_dialog_widgets_add (prefs_dialog->widgets, hue_widget);
 
   burst_repeat_widget = CHEESE_PREFS_WIDGET (cheese_prefs_burst_spinbox_new (prefs_dialog->burst_repeat,
-- 
1.7.1

